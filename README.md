# TODO-SEARCH-REACT
## _Clean Code Practice Assignment_

This code has the purpose to demonstrate some clean code practices.

## Separation of Concerns

- Built in three different layers (concerns)
- UI Components
- Business Domain (Store)
- Data Access (Gateways)

## Low Coupling and High Cohesion

- Data coupling (low coupling)
- Functional cohesion
- Different elements of a module cooperate to achieve a single function

## Patterns

- Low coupling
- high cohesion
- polymorphism

## SOLID, DRY, KISS, YAGNI and others

- Single responsibility
- Open-Closed principle
- DRY
- KISS

## Installation

todo-search-react requires [Node.js](https://nodejs.org/) v10+ to run.

Install the dependencies and start the server.

```sh
cd todo-search-react
yarn install
yarn start
```

## License

MIT