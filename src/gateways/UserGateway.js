/**
 * Built in three different layers (concerns).
 * Gateway Layer
 * Single responsibility
 */
export default function UserGateway(url){
  
    function get() {
      return fetch(url).then(toJson);
    }
    
    function toJson(response){
      return response.json();
    }
    
    return  Object.freeze({
        get
    });
}