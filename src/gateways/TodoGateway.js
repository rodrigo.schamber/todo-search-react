/**
 * Built in three different layers (concerns).
 * Gateway Layer
 * Single responsibility
 */
export default function TodoGateway(url){
    
    function get() {
      return fetch(url).then(toJson);
    }
    
    function toJson(response){
      return response.json();
    }
    
    function add(todo) {
      return fetch(url, {
        method: "POST",
        body: JSON.stringify(todo),
      }).then(toJson);
    }
    
    return  Object.freeze({
      get,
      add
    });
}