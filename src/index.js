import React from 'react';
import ReactDOM from 'react-dom';
import TodoGateway from "./gateways/TodoGateway";
import UserGateway from "./gateways/UserGateway";
import TodoStore from "./stores/TodoStore";
import UserStore from "./stores/UserStore";
import TodoContainer from "./components/TodoContainer";
import 'bootstrap/dist/css/bootstrap.min.css';

(function startApplication(){
  /**
   * Data coupling (Low Coupling pattern)
   * Functional Cohesion (High Cohesion pattern)
   * * DRY and KISS principles applied.
   */
  const userGateway = UserGateway("https://jsonplaceholder.typicode.com/users");
  const todoGateway = TodoGateway("https://jsonplaceholder.typicode.com/todos");
  const userStore = UserStore(userGateway);
  const todoStore = TodoStore(todoGateway, userStore);
  
  const stores = {
    todoStore,
    userStore
  };

  function loadStaticData(){
    return Promise.all([userStore.fetch()]);
  }

  function mountPage(){
    ReactDOM.render(
      <TodoContainer stores={stores} />,
      document.getElementById('root'));
  }

  loadStaticData().then(mountPage);
})();
