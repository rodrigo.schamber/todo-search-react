/**
 * Built in three different layers (concerns).
 * UI Layer
 */
import React from 'react';
import TodoListItem from './TodoListItem';
import PropTypes from 'prop-types';
import {ListGroup} from 'react-bootstrap';

export default function TodoList(props) {
    function renderTodoItem(todo){
      return <TodoListItem todo={todo} key={todo.id}></TodoListItem>;
    }
  
    return(
        <ListGroup>
            { props.todos.map(renderTodoItem) }
        </ListGroup>
    );
}

TodoList.propTypes = {
    todos: PropTypes.array
};