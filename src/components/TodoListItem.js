/**
 * Built in three different layers (concerns).
 * UI Layer
 */
import React from 'react';
import PropTypes from 'prop-types';
import {ListGroup} from 'react-bootstrap';
import TodoElementColored from "./TodoElementColored";
import LabelImportantIcon from '@material-ui/icons/LabelImportant';

export default function TodoListItem(props){
    let element = new TodoElementColored(props.todo.title, props.todo.userName);
    return (
        <ListGroup.Item>
            <h6>
                <LabelImportantIcon
                    style={{
                        fontSize: 40,
                        color: element.color
                    }}
                />
                <u>{`${element.title}`}</u>
            </h6>
            <h6><i>{element.author}</i></h6>
        </ListGroup.Item>
    );
}

TodoListItem.propTypes = {
    todo: PropTypes.object
}