/**
 * Built in three different layers (concerns).
 * UI Layer
 * * Single responsibility
 */
import React from 'react';
import PropTypes from 'prop-types';
import {Button, Col, Form} from 'react-bootstrap';

export default class TodoSearch extends React.Component{
    constructor(props){
        super(props);
        this.search = this.search.bind(this);
        this.handleChange = this.handleChange.bind(this);
    
        this.state = { text: "" };
    }
      
    search(){
        const query = Object.freeze({ text: this.state.text });
        if(this.props.onSearch)
          this.props.onSearch(query);
    }
      
    handleChange(event) {
        this.setState({text: event.target.value});
    }
    
    render() {
        return (
            <Form>
                <Form.Row className="align-items-center">
                    <Col xs="auto">
                        <Form.Control
                            onChange={this.handleChange}
                            placeholder="To do..."
                            value={this.state.text}
                        />
                    </Col>
                    <Col xs="auto">
                        <Button type="button" onClick={this.search}>
                            Search
                        </Button>
                    </Col>
                </Form.Row>
            </Form>
        );
    }
}

TodoSearch.propTypes = {
    onSearch: PropTypes.func
};