/**
 * Built in three different layers (concerns).
 * UI Layer
 */
import React from 'react';
import PropTypes from 'prop-types';
import TodoList from './TodoList';
import TodoSearch from './TodoSearch';
import {Col, Container, Image, Row} from 'react-bootstrap';
import todoBanner from '../assets/jpg/todo.jpg';
  
export default class TodoContainer extends React.Component {
  
  constructor(props){
    super(props);
    this.todoStore = props.stores.todoStore;
    this.search = this.search.bind(this);
    this.reload = this.reload.bind(this);

    this.query = null;
    this.state = {
      todos: []
    };
  }
  
  componentDidMount(){
    this.todoStore.onChange(this.reload);
    this.todoStore.fetch();
  }
  
  reload(){
    const todos = this.todoStore.getBy(this.query);
    this.setState({ todos });
  }
  
  search(query){
    this.query = query;
    this.reload();
  }
  
  render() {
    return (
      <Container>
        <Row className="justify-content-md-center">
          <Col md={4}>
            <Image src={todoBanner} fluid/>
          </Col>
        </Row>
        <br></br>
        <Row>
        <Col>
          <TodoSearch onSearch={this.search} />
        </Col>
        </Row>
        <br></br>
        <Row>
          <Col>
            <TodoList todos={this.state.todos} />
          </Col>
        </Row>
      </Container>
    );
  }
}

TodoContainer.propTypes = {
  stores: PropTypes.object
};