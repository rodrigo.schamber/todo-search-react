/**
 * Built in three different layers (concerns).
 * UI Layer
 * Open-Close Principle
 */
export default class TodoElement {
    constructor(title, author) {
        this.title = title;
        this.author = author;
    }
}