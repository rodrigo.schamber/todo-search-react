/**
 * Built in three different layers (concerns).
 * UI Layer
 * Polymorphysm pattern
 * Open-Close Principle
 */
import TodoElement from './TodoElement';

export default class TodoElementColored extends TodoElement {
    constructor(title, author){
        super(title, author);
        this.color = `#${Math.floor(Math.random()*16777215).toString(16)}`;
    }
}